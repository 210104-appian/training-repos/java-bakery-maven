package dev.rehm.services;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import dev.rehm.models.BakeryItem;
import dev.rehm.models.Bread;
import dev.rehm.models.Muffin;
import dev.rehm.models.Order;

public class CalculationServiceTest {
	
	/*
	 * @Before - runs before each test
	 * @BeforeClass - runs once before the class runs
	 * @After - runs after each test 
	 * @AfterClass - runs once after the entire class runs
	 * @Ignore - skips method with this annotation
	 */
	private CalculationService calculationService = new CalculationService();

	@Test(expected = IllegalArgumentException.class)
	public void testWithNullOrder() {
		calculationService.calculateOrderSubtotal(null);
	}
	
	@Test
	public void testOrderWithNullItems() {
		double expected = 0.0;
		Order order = new Order(5,null);
		double actual = calculationService.calculateOrderSubtotal(order);
		assertEquals(expected, actual, 0.0);
	}
	
	@Test
	public void testOrderWithNoItems() {
		double expected = 0.0;
		Order order = new Order(5,new ArrayList<>());
		double actual = calculationService.calculateOrderSubtotal(order);
		assertEquals(expected, actual, 0.0);
	}
	
	@Test
	public void testOrderWithValidItems() {
		ArrayList<BakeryItem> items = new ArrayList<>();
		items.add(new Muffin(3.00, "Blueberry"));
		items.add(new Bread("Wheat",4.00));
		items.add(new Bread("White",4.00));
		Order order = new Order(6,items);
		double expected = 11.00;
		double actual = calculationService.calculateOrderSubtotal(order);
		assertEquals(expected, actual,0.0);
	}
	
}
