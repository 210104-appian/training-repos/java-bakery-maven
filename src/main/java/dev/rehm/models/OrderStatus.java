package dev.rehm.models;

public enum OrderStatus {
	
	PENDING, CANCELED, COMPLETED

}
