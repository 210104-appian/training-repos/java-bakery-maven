package dev.rehm.models;

import java.util.ArrayList;
import java.util.Arrays;

public class Order {
	
	private int orderNumber;
	private String customerName;
	private String phone;
	private boolean hasPrepaid;
	private OrderStatus status;
	private ArrayList<BakeryItem> items;
	
	public Order() {
		super();
	}
	
	public Order(OrderStatus status) {
		super();
		this.status = status;
	}
	
	public Order(int orderNumber, ArrayList<BakeryItem> item) {
		super();
		this.items = item;
	}

	public Order(int orderNumber, String customerName, String phone, boolean hasPrepaid, OrderStatus status,
			ArrayList<BakeryItem> item) {
		super();
		this.orderNumber = orderNumber;
		this.customerName = customerName;
		this.phone = phone;
		this.hasPrepaid = hasPrepaid;
		this.status = status;
		this.items = item;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isHasPrepaid() {
		return hasPrepaid;
	}

	public void setHasPrepaid(boolean hasPrepaid) {
		this.hasPrepaid = hasPrepaid;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public ArrayList<BakeryItem> getItems() {
		return items;
	}

	public void setItems(ArrayList<BakeryItem> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customerName == null) ? 0 : customerName.hashCode());
		result = prime * result + (hasPrepaid ? 1231 : 1237);
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + orderNumber;
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (customerName == null) {
			if (other.customerName != null)
				return false;
		} else if (!customerName.equals(other.customerName))
			return false;
		if (hasPrepaid != other.hasPrepaid)
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (orderNumber != other.orderNumber)
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Order [orderNumber=" + orderNumber + ", customerName=" + customerName + ", phone=" + phone
				+ ", hasPrepaid=" + hasPrepaid + ", status=" + status + ", items=" + items + "]";
	}

}
