package dev.rehm.models;

public class Coffee extends BakeryItem {

	private Size size;
	private String roast;
	
	public Coffee() {
		super();
	}
	
	public Coffee(Size size, String roast, double price) {
		super(price);
		this.size = size;
		this.roast = roast;
	}
	
	public Size getSize() {
		return size;
	}

	public void setSize(Size size) {
		this.size = size;
	}

	public String getRoast() {
		return roast;
	}

	public void setRoast(String roast) {
		this.roast = roast;
	}

	public void purchaseItem() {
		System.out.println("purchasing coffee for $"+price);
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((roast == null) ? 0 : roast.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coffee other = (Coffee) obj;
		if (roast == null) {
			if (other.roast != null)
				return false;
		} else if (!roast.equals(other.roast))
			return false;
		if (size != other.size)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Coffee [size=" + size + ", roast=" + roast + ", price="+ price +"]";
	}
	
	

}
