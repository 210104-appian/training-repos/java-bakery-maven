package dev.rehm.models;

public class Bread extends BakeryItem {
	
	protected String type;
	
	/*
	 * Whether you plan to use the no argument constructor yourself, it's good practice to include one 
	 * in every class - this prevents "breaking the constructor chain" and it is helpful for working with 
	 * frameworks as well, as they often will create objects for you using no argument constructor
	 */
	public Bread(){
		super(); // making a call to the Object class's constructor
	}
	
	/*
	 * Here we see multiple constructors, with different sets of parameters. This is constructor overloading
	 * and it's an example of static polymorphism (compile-time polymorphism) - the approprirate implementation
	 * is determined at compiletime
	 */
	public Bread(String type, double price){
		super(price);
		this.type = type;
	}
	
	public void setType(String type) {
		if(type!=null) {
			this.type = type;			
		}
	}
	
	public String getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bread other = (Bread) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	/*
	 * Here we see method overriding, we created an implementation of the toString method 
	 * which is more appropriate for the class. Method overriding is an example of 
	 * dynamic polymorphism (runtime polymorphism) - the appropriate implementation is not actually
	 * determined until runtime
	 */
	@Override
	public String toString() {
//		return "Bread [type=" + this.type + "]";
		return "Bread [type=" + type + ", price="+price+"]";
	}

	@Override
	public void purchaseItem() {
		System.out.println("purchasing loaf of bread for $"+price);
		
	}

	
	
}
